/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.magnum.mobilecloud.video;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.repository.Video;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import retrofit.http.Body;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class VideoController {
    @Autowired
    private VideoRepository vRepo;

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.GET)
    public @ResponseBody
    Collection<Video> getVideoList() {
        List<Video> l = new ArrayList<Video>();
        Iterable<Video> it = vRepo.findAll();
        it.forEach(l::add);
        return l;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Video getVideoById(@PathVariable("id") long id, HttpServletResponse response) {
        //can't send null to findOne so no exception.
        Video v = vRepo.findOne(id);
        if (null == v) response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return v;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
    public @ResponseBody
    Video addVideo(@RequestBody Video v) {
        Video n = new Video(v.getName(), v.getUrl(), v.getDuration(), 0L);
        vRepo.save(n);
        return n;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/like", method = RequestMethod.POST)
    public @ResponseBody
    Void likeV(@PathVariable("id") long id, HttpServletResponse response, Principal p) {
        Video v = vRepo.findOne(id);
        int rv;
        if (null == v) {
            rv = HttpServletResponse.SC_NOT_FOUND;
        } else {
            String userName = p.getName();
            if (v.addOneLike(userName)) //success
            {
                rv = HttpServletResponse.SC_OK;
                vRepo.save(v);
            } else {
                rv = HttpServletResponse.SC_BAD_REQUEST;
            }
        }
        response.setStatus(rv);
        return null;
    }


    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/unlike", method = RequestMethod.POST)
    public @ResponseBody
    Void unLikeV(@PathVariable("id") long id, HttpServletResponse response, Principal p) {
        Video v = vRepo.findOne(id);
        int rv;
        if (null == v) {
            rv = HttpServletResponse.SC_NOT_FOUND;
        } else {
            String userName = p.getName();
            if (v.removeOneLike(userName)) {
                rv = HttpServletResponse.SC_OK;
                vRepo.save(v);
            } else {
                rv = HttpServletResponse.SC_BAD_REQUEST;
            }
        }
        response.setStatus(rv);
        return null;
    }


    @RequestMapping(value = VideoSvcApi.VIDEO_TITLE_SEARCH_PATH, method = RequestMethod.GET)
    public @ResponseBody
    Collection<Video> findByTitle(@RequestParam(VideoSvcApi.TITLE_PARAMETER) String title) {
        return vRepo.findByName(title);
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_DURATION_SEARCH_PATH, method = RequestMethod.GET)
    public @ResponseBody
    Collection<Video> findByDurationLessThan(@RequestParam(VideoSvcApi.DURATION_PARAMETER) long duration) {
        return vRepo.findByDurationLessThan(duration);
    }
}
