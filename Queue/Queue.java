/* implementation of linked list queue */
public class Queue<T> {
    private Node<T> first = null;
    private Node<T> last = null;
    private long numOfNodes = 0;

    public void enqueue(T t){
        Node<T> temp = new Node(t);
        if (isEmpty()) {
            first = temp;
        }else{
            last.setNext(temp);
        }
        last =temp;
        ++numOfNodes;
    }

    public T dequeue(){
        if (!isEmpty()){
            Node<T> temp = first;
            first = temp.getNext();
            --numOfNodes;
            return temp.getValue();
        }
        return null;
    }

    public boolean isEmpty(){
        return 0 == numOfNodes;
    }

    public void forEach(Command_Node<T> cmdNode){
        Node<T> cur = first;
        for (long i = 0; i < numOfNodes; ++i){
            cmdNode.execute(cur);
            cur = cur.getNext();
        }
    }
}
