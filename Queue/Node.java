/* data shall get reference to existed object and won't instance a new pbject */
public class Node<T> {
    private T data;
    private Node<T> next;

    // CTOR
    public Node(T t){
        data = t;
        next = null;
    }

    // setter to next node
    public void setNext(Node<T> n){
        next = n;
    }

    // setter to node's value
    public void setValue(T t){
        data = t;
    }

    // getter to next node
    public Node<T> getNext(){
        return next;
    }

    // getter to node's value
    public T getValue(){
        return data;
    }

}