public interface Command_Node<T> extends Command<Node<T>> {
    @Override
    void execute(Node<T> node);
}
