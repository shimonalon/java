// tester of Queue
final class Main {
    public static void main(String[] args) {
        Queue<Integer> q = new Queue<Integer>();
        Command_Node<Integer> printIntCmd = new PrintIntCmd();
        Command_Node<Integer> mulIntCmd = new MultiplyIntCmd();
        Integer[] nums = {8, 9, 10, 11, 12, 13, 14, 22};

        for (Integer i : nums) {
            q.enqueue(i);
        }

        q.forEach(printIntCmd);
        System.out.println("");

        q.forEach(mulIntCmd);
        q.forEach(printIntCmd);
        System.out.println("");

        for (int j = 3; j < nums.length; ++j) { // 3 elements will remain in queue
            q.dequeue();
        }

        q.forEach(printIntCmd);
        System.out.println("");
    }

    // inner class is static so we won't need to create outer class
    private static class PrintIntCmd implements Command_Node<Integer> {
        @Override
        public void execute(Node<Integer> node) {
            System.out.print(node.getValue() + " ");
        }
    }

    // inner class is static so we won't need to create outer class
    private static class MultiplyIntCmd implements Command_Node<Integer> {
        @Override
        public void execute(Node<Integer> node) {
            node.setValue(2 * node.getValue());
        }
    }
}