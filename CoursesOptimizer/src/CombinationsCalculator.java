import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CombinationsCalculator
{
    AllCombinations allComb;
    public CombinationsCalculator(AllCombinations allCombinations)
    {
        allComb = allCombinations;
    }

    public Combination GetCombinationById(int i)
    {
        return allComb.combinationsArr[i];
    }

    public int getMinNumDays_index()
    {
        int mini = Integer.MAX_VALUE;
        int ans = -1;
        for (int i = 0; i < allComb.getNumCombinations(); ++i)
        {
            if(mini > allComb.getCombinationsArr()[i].GetNDays())
            {
                mini = allComb.getCombinationsArr()[i].GetNDays();
                ans = i;
            }
        }
        return ans;
    }

    public int getMinNumWindows_index()
    {
        double mini = Double.MAX_VALUE;
        int ans = -1;
        for (int i = 0; i < allComb.getNumCombinations(); ++i)
        {
            if(mini > allComb.getCombinationsArr()[i].GetNumWindows())
            {
                mini = allComb.getCombinationsArr()[i].GetNumWindows();
                ans = i;
            }
        }
        return ans;
    }

    public int getMinAvgWindows_index()
    {
        double mini = Double.MAX_VALUE;
        int ans = -1;
        for (int i = 0; i < allComb.getNumCombinations(); ++i)
        {
            if(mini > allComb.getCombinationsArr()[i].GetAvgWindows())
            {
                mini = allComb.getCombinationsArr()[i].GetAvgWindows();
                ans = i;
            }
        }
        return ans;
    }

    public int getMaxAvgNeto_index()
    {
        double maxi = Double.MIN_VALUE;
        int ans = -1;
        for (int i = 0; i < allComb.getNumCombinations(); ++i)
        {
            if(maxi > allComb.getCombinationsArr()[i].getAvgNeto())
            {
                maxi = allComb.getCombinationsArr()[i].getAvgNeto();
                ans = i;
            }
        }
        return ans;
    }

    /*
    The main logic of optimization:
        1. Filter combinations that have above minimum days.
        2. Filter combinations that have above minimum windows.
        3. Filter combinations that have above minimum deviation of number of hours.
        Returns the remained combinations.
    */
    public OptimizationThreeMins getOptimization()
    {
        int minimumDays = Integer.MAX_VALUE;
        List<Integer> arrayList = new ArrayList<Integer>();

        // Filter 1
        for (int i = 0; i < allComb.getNumCombinations(); ++i)
        {
            int curNumDays = allComb.getCombinationsArr()[i].GetNDays();
            if(minimumDays > curNumDays)
            {
                minimumDays = curNumDays;
                arrayList.clear();
                arrayList.add(i);
            }
            else if(minimumDays == curNumDays)
            {
                arrayList.add(i);
            }
            else
            {
                // do nothing
            }
        }

        // Filter 2
        double minimumWindowsInMinDays = Double.MAX_VALUE;
        for(int i = 0; i < arrayList.size(); ++i)
        {
            double curNumWindows = allComb.getCombinationsArr()[arrayList.get(i)].GetNumWindows();
            if(minimumWindowsInMinDays > curNumWindows)
            {
                minimumWindowsInMinDays = curNumWindows;
            }
        }

        Iterator<Integer> it = arrayList.iterator();
        while(it.hasNext())
        {
            Integer combId = it.next();
            if(allComb.getCombinationsArr()[combId].GetNumWindows() > minimumWindowsInMinDays)
            {
                it.remove();
            }
        }



        //Filter 3
        double minimumDeviation =Double.MAX_VALUE;
        for(int i = 0; i < arrayList.size(); ++i)
        {
            double curDeviation = allComb.getCombinationsArr()[arrayList.get(i)].getDeviationOfNumHours();
            if(minimumDeviation > curDeviation)
            {
                minimumDeviation = curDeviation;
            }
        }

        it = arrayList.iterator();
        while(it.hasNext())
        {
            Integer combId = it.next();
            if(allComb.getCombinationsArr()[combId].getDeviationOfNumHours() > minimumDeviation)
            {
                it.remove();
            }
        }


        // Finish
        int[] in = new int[arrayList.size()];
        for (int i = 0 ; i< arrayList.size(); ++i)
        {
            in[i] = arrayList.get(i).intValue();
        }

        OptimizationThreeMins mm = new OptimizationThreeMins(in,minimumDays, minimumWindowsInMinDays, minimumDeviation);
        return mm;
    }

}
