import java.util.ArrayList;
import java.util.List;

public class Group
{
    private List<Date> dates;
    private Integer groupNum;
    private int[] hoursByType;

    public Group(Integer num)
    {
        dates = new ArrayList<Date>();
        groupNum = num;
    }

    public Integer GetGroupNum()
    {
        return groupNum;
    }

    public void addDate(Date d)
    {
        dates.add(d);
    }

    public List<Date> GetDates()
    {
        return dates;
    }

    public int[] getHoursByType()
    {
        return hoursByType;
    }


    public void CalculateHoursByType()
    {
        hoursByType = new int[3]; //3 types
        for(Date d: dates)
        {
            int typeNum =d.getTypeNumber().intValue();
            switch(typeNum)
            {
                case 1:
                    hoursByType[0] += d.getNumHours();
                    break;
                case 2:
                    hoursByType[1] += d.getNumHours();
                    break;
                case 3:
                    hoursByType[2] += d.getNumHours();
                    break;
                default:
                    throw new RuntimeException("error in type number " + typeNum);
            }

        }
    }

}
