/*
       Three minimums are ( according to their priority):
       1. Number of days a week.
       2. Number of windows a week.
       3. Deviation of  number of hours a day.
 */

public class OptimizationThreeMins
{
    final private int[] indexes;
    final private int minimumDays;
    final private double minimumWindows;
    final private double minimumDev;

    public OptimizationThreeMins(int[] in, int minDays, double minWind, double minDev)
    {
        indexes = in;
        minimumDays = minDays;
        minimumWindows = minWind;
        minimumDev = minDev;
    }

    public double getMinimunWindows() {
        return minimumWindows;
    }

    public int getMinimumDays() {
        return minimumDays;
    }

    public double getMinimunDev(){
        return  minimumDev;
    }


    public int[] getIndexes() {
        return indexes;
    }
}
