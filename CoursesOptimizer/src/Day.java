public class Day
{
    private Integer beginHour = 0;
    private Integer endHour = 0;
    private Integer dayNum = 0;
    private double nHoursNeto = 0;


    public double getNumWindows()
    {
        int diff = endHour.intValue() - beginHour.intValue();
        double numHoursBruto =  (double)(diff/100) + ((double)(diff%100))/60.0;
        return numHoursBruto - nHoursNeto;
    }

    public double getnHoursNeto() {
        return nHoursNeto;
    }

    public void setnHoursNeto(double nHoursNeto) {
        this.nHoursNeto = nHoursNeto;
    }

    public Integer getDayNum() {
        return dayNum;
    }

    public void setDayNum(Integer dayNum) {
        this.dayNum = dayNum;
    }

    public void setBeginHour(Integer beginHour) {
        this.beginHour = beginHour;
    }

    public Integer getEndHour() {
        return endHour;
    }

    public Integer getBeginHour() {
        return beginHour;
    }

    public void setEndHour(Integer endHour) {
        this.endHour = endHour;
    }
}

