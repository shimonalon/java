import java.util.HashMap;
import java.util.Map;

public class Main
{
    public static void main(String[] args)
    {
        try
        {
            Map<Integer, Course> courses = new HashMap<Integer, Course>();

            Readsheet reader = new Readsheet();
            reader.ReadCoursesFromExcel("courses.xlsx", courses);
            reader.ValidateCoursesPointFromExcel("courses.xlsx", courses);

            AllCombinations allComb = new AllCombinations(courses);
            CombinationsCalculator calc = new CombinationsCalculator(allComb);

            OptimizationThreeMins mmm = calc.getOptimization();
            int[] ind = mmm.getIndexes();
            Combination[] comb = new Combination[ind.length];
            for(int i = 0; i < comb.length; ++i)
            {
                comb[i] = calc.GetCombinationById(ind[i]);
            }

            WriteCombination writeCombination = new WriteCombination(comb);
            writeCombination.printToExcel("outFile.xlsx");

        }
        catch(Exception e)
        {
            System.out.println("Exception caught " + e);
        }

    }
}
