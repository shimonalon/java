public class Date
{
    private Course course;
    private Integer subNum;
    private Integer type; //lecture, exercise
    private Integer day;
    private Integer beginHour;
    private Integer endHour;
    private String building;
    private String room;
    private double numHours;

    public Date(Integer subNum, Integer type, Integer day, Integer begin, Integer end, String building, String room)
    {
        this.subNum = subNum;
        this.type = type;
        this.day = day;
        beginHour = begin;
        endHour = end;
        this.building = building;
        this.room = room;
        int diff = endHour.intValue() - beginHour.intValue();
        if(diff <= 0) throw new RuntimeException("end hour has to be greate than begin hour");
        numHours = (double)(diff/100) + ((double)(diff%100))/60.0;
    }

    public Integer getBeginHour() {
        return beginHour;
    }

    public Integer getEndHour() {
        return endHour;
    }

    public Integer getDay() {
        return day;
    }

    public void setCourse(Course crs)
    {
        course = crs;
    }

    public Course getCourse()
    {
        return course;
    }

    public double getNumHours() {
        return numHours;
    }

    public Integer getTypeNumber() {
        return type;
    }
}
