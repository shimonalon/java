import java.lang.reflect.Array;
import java.util.*;

public class AllCombinations
{
    private int nCombinations = 0;
    Combination[] combinationsArr;

    public int getNumCombinations()
    {
        return  combinationsArr.length;
    }

    public Combination[] getCombinationsArr()
    {
        return combinationsArr;
    }

    public AllCombinations(Map<Integer, Course> courses)
    {
        int[] indexArr = new int[courses.size()];

        Course[] crsArr = new Course[courses.size()];
        int[] nGroupsInCourse = new int[courses.size()];
        int i = 0;
        Iterator it = courses.entrySet().iterator();

        int nComb = 0;
        for (int crsIndex=0; it.hasNext(); ++crsIndex)
        {
            Map.Entry mapElem = (Map.Entry)it.next();
            crsArr[crsIndex] = (Course)mapElem.getValue();
            nGroupsInCourse[crsIndex] =  crsArr[crsIndex].getSize();
            if  (crsIndex == 0)
            {
                nComb = nGroupsInCourse[crsIndex];
            }
            else
            {
                nComb *= nGroupsInCourse[crsIndex];
            }
        }

        combinationsArr = new Combination[nComb];
        InitializeAllCombinations(indexArr,  nGroupsInCourse, crsArr, courses.size() - 1, courses.size());
        if (nCombinations != nComb)
        {
            throw new RuntimeException("some Error");
        }
        DeleteCombinationsWithCollision();
    }

    private void InitializeAllCombinations(int[] arr, int[] maxArr, Course[] crsArr, int p, int nCourses)
    {
        if(p == 0 )
        {
            for(int i = 0;  i < maxArr[p] ; ++i)
            {
                SetCombination(arr, crsArr, nCourses);
                arr[p] = (arr[p] + 1) % maxArr[p];
            }
        }
        else
        {
            for (int i = 0; i <= maxArr[p] - 1; ++i)
            {
                InitializeAllCombinations(arr, maxArr, crsArr,  p -1, nCourses);
                arr[p] = (arr[p] + 1) % maxArr[p];
            }
        }
    }

    private void SetCombination(int[] arr, Course[] crsArr, int nCourses)
    {
        Group[] grpArr = new Group[nCourses];
        for(int i = 0; i < nCourses; ++i)
        {
            grpArr[i] = crsArr[i].GetByIndex(arr[i]);
        }
        combinationsArr[nCombinations++] = new Combination(grpArr);
    }


    private void DeleteCombinationsWithCollision()
    {
        List<Combination> tmp = new ArrayList<Combination>();

        for(int i=0; i < combinationsArr.length;++i)
        {
            if(combinationsArr[i].isValidComibation())
            {
                tmp.add(combinationsArr[i]);
            }
            else
            {
                // will not get inside the list
            }
        }

        combinationsArr = new Combination[tmp.size()];
        combinationsArr = tmp.toArray(combinationsArr);
    }

}
