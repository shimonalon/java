import java.util.*;

public class Combination
{
    final private Group[] grpArr;
    private int nDaysInWeek;
    private double nWindows;
    private double avgWindows;
    private double avgNeto;
    private boolean isValidComibation;
    private double deviationOfNumHours;
    private List<Integer> hours; // sorted and not duplicated

    public Combination(Group[] groupArr)
    {
        this.grpArr = groupArr;
        InitHours();
        InitWindowsAndDays();
    }

    public double getDeviationOfNumHours()
    {
        return deviationOfNumHours;
    }

    public Group[] GetGroups()
    {
        return grpArr;
    }

    public int GetNDays()
    {
        return nDaysInWeek;
    }

    public boolean isValidComibation() {
        return isValidComibation;
    }

    public double GetNumWindows()
    {
        return nWindows;
    }

    public double GetAvgWindows()
    {
        return avgWindows;
    }

    public double getAvgNeto() {
        return avgNeto;
    }

    public List<Integer> GetSortedHours()
    {
        return hours;
    }

    private void InitHours()
    {
        hours = new ArrayList<>();
        for(int i = 0; i < grpArr.length; ++i)
        {
            List<Date> dates = grpArr[i].GetDates();
            for(int j = 0; j< dates.size(); ++j)
            {
                Date d = dates.get(j);
                if(false == hours.contains(d.getBeginHour())) hours.add(d.getBeginHour());
                if(false == hours.contains(d.getEndHour())) hours.add(d.getEndHour());
            }
        }
        Collections.sort(hours);
    }

    private void InitWindowsAndDays()
    {
        Map<Integer, Day> m = new HashMap<>();
        for(int i = 0; i < grpArr.length; ++i)
        {
            List<Date> dates = grpArr[i].GetDates();
            for(int j = 0; j< dates.size(); ++j)
            {
                Date d = dates.get(j);
                Day dia = m.get(d.getDay());
                Util_InitWindowsAndDays(m, d, dia);
            }
        }
        Util_ValidateDays(m);
        if(isValidComibation)Util_InitStatistics(m);
    }


    private void Util_InitStatistics(Map<Integer, Day> m)
    {
        nDaysInWeek = 0;
        nWindows = 0;
        double neto = 0.0;
        for (Map.Entry<Integer, Day> entry : m.entrySet())
        {
            ++nDaysInWeek;
            nWindows += entry.getValue().getNumWindows();
            neto += entry.getValue().getnHoursNeto();
        }
        avgWindows = nWindows / nDaysInWeek;
        avgNeto = neto / nDaysInWeek;

        deviationOfNumHours = 0.0;
        for (Map.Entry<Integer, Day> entry : m.entrySet())
        {
            deviationOfNumHours += Math.pow(entry.getValue().getnHoursNeto() - avgNeto, 2);
        }
        deviationOfNumHours = Math.sqrt(deviationOfNumHours);
    }

    private void Util_InitWindowsAndDays(Map<Integer, Day> m, Date d, Day dia)
    {
        if(null == dia)
        {
            dia = new Day();
            dia.setDayNum(d.getDay());
            dia.setBeginHour(d.getBeginHour());
            dia.setEndHour(d.getEndHour());
            dia.setnHoursNeto(d.getNumHours());
            m.put(dia.getDayNum(), dia);
        }
        else
        {
            if(dia.getBeginHour() > d.getBeginHour())
            {
                dia.setBeginHour(d.getBeginHour());
            }
            if(dia.getEndHour() < d.getEndHour())
            {
                dia.setEndHour(d.getEndHour());
            }
            dia.setnHoursNeto(dia.getnHoursNeto() + d.getNumHours());
        }
    }

    private void Util_ValidateDays(Map<Integer, Day> m)
    {
        isValidComibation = true;

        List<Date> allDatesInCombination = new ArrayList<Date>();
        for(Group g: grpArr)
        {
            allDatesInCombination.addAll(g.GetDates());
        }

        int size = allDatesInCombination.size();
        for(int i = 0; isValidComibation && i < size - 1; ++i)
        {
            for (int j= i +1 ; j < size; ++j)
            {
                if(Util2_IsCollision(allDatesInCombination.get(i), allDatesInCombination.get(j)))
                {
                    isValidComibation = false;
                    break;
                }
            }
        }
    }

    private boolean Util2_IsCollision(Date a, Date b)
    {
        boolean isCollision = false;
        if (a.getDay().intValue() == b.getDay().intValue())
        {
            if((a.getEndHour() <= b.getBeginHour()) ||
                    (b.getEndHour() <= a.getBeginHour()))
            {
                // definitely no collision.
            }
            else
            {
                isCollision = true;
            }
        }
        else
        {
            // definitely no collision.
        }

        return isCollision;
    }
}
