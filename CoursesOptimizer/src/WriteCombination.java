import java.io.File;
import java.io.FileOutputStream;

import java.util.*;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import javax.swing.*;

public class WriteCombination
{
    private XSSFWorkbook wb;
    private CellStyle styleForeground_1;
    private CellStyle styleForeground_2;
    private CellStyle styleForeground_3;
    private CellStyle styleForeground_4;
    private CellStyle styleForeground_5;
    private final int nColumns = 8;

    public WriteCombination(Combination[] comb)
    {
        wb = new XSSFWorkbook(); //Create blank workbook
        InitStyles();

        if (0 == comb.length)
        {
            WriteNoComibationsFound();
        }
        for(int i = 0; i < comb.length;++i)
        {
            XSSFSheet spreadsheet = wb.createSheet("combination " + Integer.toString(i)); //Create a blank sheet
            WriteOneSpreadSheet(spreadsheet, comb[i]);
        }

    }

    public void printToExcel(String outFileName) throws Exception
    {
        FileOutputStream out = new FileOutputStream(new File(outFileName));
        wb.write(out);
        out.close();
    }



    private void InitStyles()
    {
        Font boldFont = wb.createFont();
        boldFont.setBold(true);

        styleForeground_1 = wb.createCellStyle();
        styleForeground_1.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        styleForeground_1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleForeground_1.setAlignment(HorizontalAlignment.CENTER);
        styleForeground_1.setFont(boldFont);

        styleForeground_2 = wb.createCellStyle();
        styleForeground_2.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        styleForeground_2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleForeground_2.setAlignment(HorizontalAlignment.CENTER);
        styleForeground_2.setFont(boldFont);

        styleForeground_3 = wb.createCellStyle();
        styleForeground_3.setAlignment(HorizontalAlignment.CENTER);
        styleForeground_3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleForeground_3.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        styleForeground_4 = wb.createCellStyle();
        styleForeground_4.setAlignment(HorizontalAlignment.CENTER);
        styleForeground_4.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());
        styleForeground_4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleForeground_4.setFont(boldFont);

        styleForeground_5 = wb.createCellStyle();
        styleForeground_5.setAlignment(HorizontalAlignment.CENTER);
        styleForeground_5.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        styleForeground_5.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    }

    private void WriteOneSpreadSheet(XSSFSheet spreadsheet, Combination comb)
    {
        spreadsheet.setAutobreaks(false); // column break will be set in WriteHeader. row break will be set in WriteStatsAndGroupIdsTables
        WriteHeader(spreadsheet);
        WriteHoursTable(spreadsheet, comb);
        WriteStatsAndGroupIdsTables(spreadsheet, comb);
        for(int i=0 ;i < nColumns;++i)
        {
            spreadsheet.autoSizeColumn(i, true);
        }
    }

    private void WriteHoursTable(XSSFSheet spreadsheet, Combination comb)
    {
        Group[] grpArr = comb.GetGroups();
        List<Integer> hours = comb.GetSortedHours();

        // Writing hours column
        Cell[][] cells = new Cell[hours.size()][nColumns];

        for(int i = 0; i < hours.size(); ++i)
        {
            Row row = spreadsheet.createRow( i +1);
            for(int j =0; j < nColumns; ++j)
            {
                cells[i][j] = row.createCell(j);
                if(0 == j)
                {
                    cells[i][j].setCellStyle(styleForeground_2);
                    cells[i][j].setCellValue(Integer.toString(hours.get(i)));
                }
                else
                {
                    cells[i][j].setCellStyle(styleForeground_3);
                }
            }
        }

        // Writing courses name according to hours
        int[] nHoursADay = new int[nColumns];
        for(int i = 0; i < grpArr.length; ++i)
        {
            List<Date> dates = grpArr[i].GetDates();
            for(int j = 0; j< dates.size(); ++j)
            {
                Date d = dates.get(j);
                WriteOneDate(d, cells, hours);
                nHoursADay[d.getDay().intValue()] += d.getNumHours();
            }
        }

        // Writing summary row
        Row sumRow =  spreadsheet.createRow(hours.size() + 1);
        for(int i = 0; i < nColumns; ++i)
        {
            Cell cell = sumRow.createCell(i);
            if(0 == i)
            {
                cell.setCellStyle(styleForeground_4);
                cell.setCellValue("num hours:");
            }
            else
            {
                cell.setCellStyle(styleForeground_5);
                cell.setCellValue(Integer.toString(nHoursADay[i]));
            }
        }
    }

    private void WriteStatsAndGroupIdsTables(XSSFSheet spreadsheet, Combination comb)
    {
        Group[] grpArr = comb.GetGroups();
        final int beginRowId = comb.GetSortedHours().size() + 3; // considering the table above

        final int numStat = 6;
        final int numNames = grpArr.length + 2; // 1 for header, 1 for sum
        XSSFRow[] rowsSectionB = new  XSSFRow[Integer.max(numStat, numNames)];

        for (int i = 0 ; i < rowsSectionB.length; ++i)
        {
            rowsSectionB[i] = spreadsheet.createRow(beginRowId + i);
        }

        // Writing some statistics
        String statsNamesLeft[] = {"Statistics","number of days: ","windows hours: ", "Average windows: ", "Average neto per day: ", "Dev of num hours" };
        String statsNamesRight[] = {"", Integer.toString(comb.GetNDays()),Double.toString(comb.GetNumWindows()) , Double.toString(comb.GetAvgWindows()), Double.toString(comb.getAvgNeto()), Double.toString(comb.getDeviationOfNumHours())};
        for(int i = 0; i < numStat; ++i)
        {
            Cell leftCell = rowsSectionB[i].createCell(0);
            Cell rightCell = rowsSectionB[i].createCell(2);
            leftCell.setCellValue(statsNamesLeft[i]);
            rightCell.setCellValue(statsNamesRight[i]);
            if(i == 0)
            {
                leftCell.setCellStyle(styleForeground_1);
                rightCell.setCellStyle(styleForeground_1);
            }
            else
            {
                leftCell.setCellStyle(styleForeground_2);
                rightCell.setCellStyle(styleForeground_3);
            }
            spreadsheet.addMergedRegion(new CellRangeAddress(beginRowId + i, beginRowId + i, 0, 1));
        }


        // Writing courses name and group id
        double accumulatedPoints = 0;
        for(int i = 0; i < numNames; ++i)
        {
            Cell leftCell = rowsSectionB[i].createCell(4);
            Cell midCell = rowsSectionB[i].createCell(5);
            Cell rightCell = rowsSectionB[i].createCell(6);
            if (i ==0 ) //header row
            {
                leftCell.setCellValue("Course Name");
                leftCell.setCellStyle(styleForeground_1);
                midCell.setCellValue("Group Number");
                midCell.setCellStyle(styleForeground_1);
                rightCell.setCellValue("Course Points");
                rightCell.setCellStyle(styleForeground_1);
            }
            else if(i == numNames -1 ) // sum row
            {
                leftCell.setCellValue("Total");
                leftCell.setCellStyle(styleForeground_4);
                midCell.setCellValue(numNames - 2);
                midCell.setCellStyle(styleForeground_5);
                rightCell.setCellValue(accumulatedPoints);
                rightCell.setCellStyle(styleForeground_5);
            }
            else // between them
            {
                leftCell.setCellValue(grpArr[i -1].GetDates().get(0).getCourse().getName());
                leftCell.setCellStyle(styleForeground_2);
                midCell.setCellValue(grpArr[i -1].GetGroupNum());
                midCell.setCellStyle(styleForeground_3);
                double crsPoints = grpArr[i -1].GetDates().get(0).getCourse().getCoursePoints();
                rightCell.setCellValue(crsPoints);
                rightCell.setCellStyle(styleForeground_3);
                accumulatedPoints += crsPoints;
            }
        }

        spreadsheet.setRowBreak(beginRowId + rowsSectionB.length - 1);
    }



    private void WriteHeader(XSSFSheet spreadsheet)
    {
        XSSFRow row = spreadsheet.createRow(0); //Create row object
        String[] titles =  {"hour", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Fritag", "Samstag"};

        for(int i = 0; i < titles.length; ++i)
        {
            Cell curCell = row.createCell(i);
            curCell.setCellValue(titles[i]);
            curCell.setCellStyle(styleForeground_1);
        }
        spreadsheet.setColumnBreak(titles.length);
    }

    private void WriteOneDate(Date d, Cell[][] cells,List<Integer> hours)
    {
        boolean isBeginned = false;
        for ( int k = 0; k < hours.size(); ++k)
        {
            if(!isBeginned)
            {
                if( d.getBeginHour().intValue() == hours.get(k).intValue())
                {
                    isBeginned = true;
                    --k; // in order to not miss it
                }
            }
            else
            {
                if(d.getEndHour().intValue() > hours.get(k).intValue())
                {
                    cells[k][d.getDay().intValue()].setCellValue(d.getCourse().getName());
                }
                else
                {
                    break;
                }
            }
        }
    }

    private  void WriteNoComibationsFound()
    {
        XSSFSheet spreadsheet = wb.createSheet("No Result"); //Create a blank sheet
        XSSFRow row = spreadsheet.createRow(0);
        row.createCell(0).setCellValue("Sorry, no available valid combinations ");
    }

}



