import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Course
{
    private Map<Integer, Group> groups;
    final String name;
    final Integer id;
    double points;
    private ArrayList<Group> arr;

    public Course(String name, Integer id)
    {
        groups = new TreeMap<Integer, Group>();
        arr = new ArrayList<Group>();
        this.name = name;
        this.id = id;
    }

    public void addGroup(Group g)
    {
        if( null != groups.get(g.GetGroupNum()))
        {
            throw new RuntimeException("group already exists in this course");
        }
        else
        {
            groups.put(g.GetGroupNum(), g);
            arr.add(g);
        }
    }

    // if group doesn't exist in the course, null will be returned.
    public Group GetGroup(Integer grpId)
    {
        return groups.get(grpId);
    }

    public Group GetByIndex(int i)
    {
        return arr.get(i);
    }

    public int getSize()
    {
        return arr.size();
    }


    public void setCoursePoints(double p){
        points = p;
    }

    public double getCoursePoints() {
        return points;
    }

    public String getName()
    {
        return name;
    }

    public Integer getId()
    {
        return  id;
    }
}
