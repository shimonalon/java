import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Readsheet
{
    public void ReadCoursesFromExcel(String inFileName, Map<Integer, Course> courses) throws Exception
    {
        XSSFRow row;
        FileInputStream fis = new FileInputStream(new File(inFileName));
        List<RawRow> r = new ArrayList<RawRow>();
        final int nColumns = 10;
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet spreadsheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = spreadsheet.iterator();

        // reading header row
        row = (XSSFRow) rowIterator.next();
        Iterator <Cell> cellIterator = row.cellIterator();
        while(cellIterator.hasNext())
        {
            Cell cell = cellIterator.next();
        }


        while (rowIterator.hasNext())
        {
            row = (XSSFRow) rowIterator.next();
            cellIterator = row.cellIterator();
            RawRow tempR = new RawRow();

            for(int i = 1; i <= nColumns; ++i)
            {
                Cell cell = cellIterator.next();
                switch (i)
                {
                    case 1:
                        tempR.courseId_01 = (int)cell.getNumericCellValue();
                        break;
                    case 2:
                        tempR.name_02 = cell.getStringCellValue();
                        break;
                    case 3:
                        tempR.groupNum_03 = (int)cell.getNumericCellValue();
                        break;
                    case 4:
                        tempR.subNum_04 = (int)cell.getNumericCellValue();
                        break;
                    case 5:
                        tempR.type_05 = (int)cell.getNumericCellValue();
                        break;
                    case 6:
                        tempR.day_06 = (int)cell.getNumericCellValue();
                        break;
                    case 7:
                        tempR.begin_07 = (int)cell.getNumericCellValue();
                        break;
                    case 8:
                        tempR.end_08 = (int)cell.getNumericCellValue();
                        break;
                    case 9:
                        tempR.building_09 = cell.getStringCellValue();
                        break;
                    case 10:
                        tempR.room_10 = cell.getStringCellValue();
                        break;
                }
            }
            r.add(tempR);
        }
        fis.close();
        RawRowsToCourses(r, courses);
    }


    public void  ValidateCoursesPointFromExcel(String inFileName, Map<Integer, Course> courses) throws Exception
    {
        XSSFRow row;
        FileInputStream fis = new FileInputStream(new File(inFileName));
        List<HoursByTypeRow> r = new ArrayList<HoursByTypeRow>();
        final int nColumns = 5;
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet spreadsheet = workbook.getSheetAt(1);
        Iterator<Row> rowIterator = spreadsheet.iterator();

        // reading header row
        row = (XSSFRow) rowIterator.next();
        Iterator <Cell> cellIterator = row.cellIterator();
        while(cellIterator.hasNext())
        {
            Cell cell = cellIterator.next();
        }


        while (rowIterator.hasNext())
        {
            row = (XSSFRow) rowIterator.next();
            cellIterator = row.cellIterator();
            HoursByTypeRow tempR = new HoursByTypeRow();

            for(int i = 1; i <= nColumns; ++i)
            {
                Cell cell = cellIterator.next();
                switch (i)
                {
                    case 1:
                        tempR.courseId_01 = (int)cell.getNumericCellValue();
                        break;
                    case 2:
                        tempR.type1Hours_02 = (int)cell.getNumericCellValue();
                        break;
                    case 3:
                        tempR.type2Hours_03 = (int)cell.getNumericCellValue();
                        break;
                    case 4:
                        tempR.type3Hours_04 = (int)cell.getNumericCellValue();
                        break;
                    case 5:
                        tempR.points = (double)cell.getNumericCellValue();
                        break;
                }
            }
            r.add(tempR);
        }
        fis.close();
        ChecksumValidationHoursByType(r, courses);
    }

    private void RawRowsToCourses(List<RawRow> r, Map<Integer, Course> courses)
    {
        for (RawRow row: r)
        {
            Date newDate = new Date(row.subNum_04, row.type_05, row.day_06, row.begin_07, row.end_08, row.building_09, row.room_10);
            Course c = courses.get(row.courseId_01);
            if(null != c)
            {
                Group g = c.GetGroup(row.groupNum_03);
                newDate.setCourse(c);
                if(null != g)
                {
                    g.addDate(newDate);
                }
                else
                {
                    Group newGrp = new Group(row.groupNum_03);
                    newGrp.addDate(newDate);
                    c.addGroup(newGrp);
                }
            }
            else
            {
                Course newCrs = new Course(row.name_02, row.courseId_01);
                Group newGrp = new Group(row.groupNum_03);
                newDate.setCourse(newCrs);
                newGrp.addDate(newDate);
                newCrs.addGroup(newGrp);
                courses.put(row.courseId_01, newCrs);
            }
        }
    }

    private void ChecksumValidationHoursByType(List<HoursByTypeRow> r, Map<Integer, Course> courses)
    {
        for(int i = 0; i < r.size(); ++i)
        {
            for(Map.Entry<Integer, Course> entry: courses.entrySet())
            {
                if(r.get(i).courseId_01.intValue() == entry.getKey().intValue()) // if same course id
                {
                    Course crs = entry.getValue();
                    crs.setCoursePoints(r.get(i).points);
                    for(int k = 0; k < crs.getSize(); ++k)
                    {
                        Group g = crs.GetByIndex(k);
                        g.CalculateHoursByType();
                        int[] nHoursByTypeOfGroup = g.getHoursByType();
                        if((r.get(i).type1Hours_02.intValue() != nHoursByTypeOfGroup[0]) ||
                            (r.get(i).type2Hours_03.intValue() != nHoursByTypeOfGroup[1]) ||
                            (r.get(i).type3Hours_04.intValue() != nHoursByTypeOfGroup[2])  )
                        {
                            throw new RuntimeException("sum hours by type of course " + r.get(i).courseId_01.intValue() + " is wrong.");
                        }
                    }
                }
            }
        }



    }


}
