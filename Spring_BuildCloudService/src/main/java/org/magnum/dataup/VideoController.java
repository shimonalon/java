package org.magnum.dataup;

import org.magnum.dataup.model.Video;
import org.magnum.dataup.model.VideoStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Stream;

@Controller
public class VideoController {
    private HashMap<Long, Video> vMap;
    private Long nItems = 0L;
    private VideoFileManager vfm;

    public VideoController(){
        try{
            vMap = new HashMap<Long, Video>();
            vfm= VideoFileManager.get();
        }
        catch(Exception e){
        }
    }

    @GetMapping(VideoSvcApi.VIDEO_SVC_PATH)
    public  @ResponseBody  Collection<Video> getList()
    {
        return vMap.values();
    }

    @PostMapping(VideoSvcApi.VIDEO_SVC_PATH)
    public @ResponseBody Video add(@RequestBody Video v)
    {
        v.setId(++nItems);
        vMap.put(v.getId(), v);
        String dataUrl = getDataUrl(v.getId());
        v.setDataUrl(dataUrl);
        return v;
    }

    //Cannot convert value of type
    // 'org.springframework.web.multipart.support.StandardMultipartHttpServletRequest$StandardMultipartFile' to
    // required type 'javax.servlet.MultipartConfigElement':

    @PostMapping(VideoSvcApi.VIDEO_DATA_PATH)
    public @ResponseBody VideoStatus setData(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            @RequestParam(VideoSvcApi.DATA_PARAMETER) MultipartFile vData ,
            HttpServletResponse response
            ) throws IOException
    {
        if (id > 0L && id <= nItems)
        {
            response.setStatus(HttpServletResponse.SC_OK);
            Video v = vMap.get(id);
            vfm.saveVideoData(v, vData.getInputStream());
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return new VideoStatus(VideoStatus.VideoState.READY);
    }

    @GetMapping(VideoSvcApi.VIDEO_DATA_PATH)
    void getVData(
        @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
        HttpServletResponse response) throws IOException
    {
        if (id > 0L && id <= nItems)
        {
            Video v = vMap.get(id);
            if (vfm.hasVideoData(v))
            {
                vfm.copyVideoData(v, response.getOutputStream());
                response.setStatus(HttpServletResponse.SC_OK);

            }
            else
            {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }



    /*********************************************************/
    private String getDataUrl(long videoId){
        String url = getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
        return url;
    }

    private String getUrlBaseForLocalServer() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String base =
                "http://"+request.getServerName()
                        + ((request.getServerPort() != 80) ? ":"+request.getServerPort() : "");
        return base;
    }

}


