public class VocWord
{
    private String group;
    private EnglishKey english;
    private WordTranslation[] translation; // german, spanish, russian
     private int initialIndex;

    public VocWord(String group, String eng, String tran, int transIndex)
    {
        if(transIndex < 0 || transIndex > 2) throw new IndexOutOfBoundsException();
        this.group = group;
        this.english = new EnglishKey(eng);
        this.translation = new WordTranslation[3];
        this.translation[transIndex] = new WordTranslation(tran);
        initialIndex = transIndex;
    }

    public void addTranslation(WordTranslation tran, int transIndex)
    {
        if(transIndex < 0 || transIndex > 2) throw new IndexOutOfBoundsException();
        if (this.translation[transIndex] == null)
        {
            this.translation[transIndex] = tran;
        }
        else
        {
            System.out.println("error, translation index " + transIndex + " already exist");
        }
    }

    public String getGroup()
    {
        return group;
    }

    public EnglishKey getEnglishKey()
    {
        return english;
    }

    public WordTranslation getTranslation(int transIndex)
    {
        return translation[transIndex];
    }

    public int getInitialIndex()
    {
        return initialIndex;
    }
}
