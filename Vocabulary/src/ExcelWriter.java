import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

public class ExcelWriter
{
    private Map<String, Map<EnglishKey, VocWord>> dict;


    public void setDictionary(List<VocWord> words)
    {
        dict = new HashMap<String, Map<EnglishKey, VocWord>>();

        for (VocWord vw :words )
        {
            Map<EnglishKey, VocWord> m = dict.get(vw.getGroup());
            if(null != m)
            {
                VocWord v = m.get(vw.getEnglishKey());
                if(v != null) // already exist
                {
                    int i = vw.getInitialIndex();
                    v.addTranslation(vw.getTranslation(i), i);
                }
                else
                {
                    m.put(vw.getEnglishKey(), vw);
                }

            }
            else
            {
                m = new TreeMap<EnglishKey, VocWord>();
                m.put(vw.getEnglishKey(), vw);
                dict.put(vw.getGroup(), m);
            }
        }
    }

    private void writeGroup(XSSFSheet spreadsheet, String grp, Map<EnglishKey, VocWord> m)
    {
        XSSFRow row; //Create row object

        System.out.println("grp: " + grp);

        Collection<VocWord> c = m.values();
        for (VocWord vw: c)
        {
            System.out.print(vw.getEnglishKey().get() + " = " );
            for (int i= 0 ; i < 3 ; ++i)
            {
                WordTranslation t = vw.getTranslation(i);
                if(t != null && t.get() != null)
                {
                    System.out.print( t.get() + ", ");
                }
            }
            System.out.print("\n");
        }

        //Iterate over data and write to sheet
//        Set < String > keyid = empinfo.keySet();
//        int rowid = 0;
//
//        for (String key : keyid) {
//            row = spreadsheet.createRow(rowid++);
//            Object [] objectArr = empinfo.get(key);
//            int cellid = 0;
//
//            for (Object obj : objectArr) {
//                Cell cell = row.createCell(cellid++);
//                cell.setCellValue((String)obj);
//            }
//        }
    }


    public void writeToExcel(String sheetName, String fileOutputPath) throws  Exception
    {

        XSSFWorkbook workbook = new XSSFWorkbook(); //Create blank workbook
        XSSFSheet spreadsheet = workbook.createSheet(sheetName); //Create a blank sheet

        dict.forEach((grp, m) -> writeGroup(spreadsheet, grp, m));

        //Write the workbook in file system
        FileOutputStream out = new FileOutputStream(new File(fileOutputPath));
        workbook.write(out);
        out.close();
    }



    //tester
    public static void main(String[] args)
    {
        try
        {
            ExcelWriter ew = new ExcelWriter();
            List<VocWord> wordsArr = new ArrayList<VocWord>();
            wordsArr.add(new VocWord("animals", "dog", "perro", 1));
            wordsArr.add(new VocWord("animals", " horse", "caballo", 1));
            wordsArr.add(new VocWord("animals", "    dog", "hund", 0));
            wordsArr.add(new VocWord("animals", "  cat", "kushka", 2));



           wordsArr.add(new VocWord("adj", " hello", "hallo", 0));
            wordsArr.add(new VocWord("adj", "hello", "privyet", 2));
            wordsArr.add(new VocWord("adj", " hello", "hola", 1));
            wordsArr.add(new VocWord("adj", "i am", "yo soy", 1));
            wordsArr.add(new VocWord("adj", " i am", "ich bin", 0));
            wordsArr.add(new VocWord("adj", " i ", "  ya", 2));

            ew.setDictionary(wordsArr);
            ew.writeToExcel(new String("My Sheet Name"), new String("C:\\Shimon\\voc86.xlsx"));
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

    }
}
