public class EnglishKey implements Comparable<EnglishKey>
{
    String ek;

    public EnglishKey(String s)
    {
        ek = (s.trim()).split(" ")[0];
    }

    public String get(){return ek;}

    @Override
    public int compareTo(EnglishKey o) {
        return ek.compareTo(o.get());
    }


}
