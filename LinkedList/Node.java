public class Node<T> {
	private Node<T> next;
	private T data;
	
	//CTOR
	public Node(T obj) {
		next = null;
		data = obj;
	}
	
	// setter to next node
	public void setNext(Node<T> n){
		next = n;
	}
	
	// getter to next node
	public Node<T> getNext(){
		return next;
	}
	
	// getter to node's value
	public T getValue(){
		return data;
	}	
}
