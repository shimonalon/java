//import Node
public class LinkedList<T> {
	private Node<T> root;
	
	// CTOR - objs must be already instanced in memory
	public LinkedList(T[] objs) {
		Node<T> prev = null;
		if(objs != null && objs.length > 0){
			root = new Node<T>(objs[0]);
			prev = root;
			for( int i = 1 ; i < objs.length; ++i){
				prev.setNext(new Node<T>(objs[i]));
				prev = prev.getNext();
			}				
		}	
	}
	
	//Printer of all the nodes in list
	public void printList() {
		Node<T> curr = root;
		while(null != curr )
		{
			System.out.println(curr.getValue());
			curr = curr.getNext();
		}	
	}
	
	//  tester
	public static void main (String[] args) {
		Integer[] i = {1, 5 , 8 , 9 ,10};
		LinkedList<Integer> myIntList = new LinkedList<Integer>(i);
		myIntList.printList();
	}
	
}
