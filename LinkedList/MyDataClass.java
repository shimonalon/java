//import Node
public class MyDataClass {
	private String name;
	private int age;
	
	// CTOR
	public MyDataClass(String n, int a) {
		name = n;
		age = a;
	}
	
	public void printMe(){
		System.out.println("name " + name + "age " + age  );	
	}

}
